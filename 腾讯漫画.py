import time

import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

try:
    driver = webdriver.Edge("../drivers/msedgedriver.exe")
    # 爬取1-3页数据，可自行扩展
    for i in range(1, 4):
        url = "https://ac.qq.com/Comic/index/page/" + str(i)
        driver.get(url)
        # 模拟滚动
        js = "return action=document.body.scrollHeight"
        new_height = driver.execute_script(js)
        for i in range(0, new_height, 300):
            driver.execute_script('window.scrollTo(0, %s)' % (i))
        list = driver.find_element_by_class_name('ret-search-list').find_elements_by_tag_name('li')
        data = []
        for item in list:
            imgsrc = item.find_element_by_tag_name('img').get_attribute('src')
            author = item.find_element_by_class_name("ret-works-author").text
            leixing_spanlist = item.find_element_by_class_name("ret-works-tags").find_elements_by_tag_name('span')
            leixing = leixing_spanlist[0].text + "," + leixing_spanlist[1].text
            neirong = item.find_element_by_class_name("ret-works-decs").text
            gengxin = item.find_element_by_class_name("mod-cover-list-mask").text
            itemdata = {'imgsrc': imgsrc, 'author': author, 'leixing': leixing, 'neirong': neirong, 'gengxin': gengxin}
            print(itemdata)
        data.append(itemdata)
    print(data)
    driver.close()
except Exception as e:
    print(e)
