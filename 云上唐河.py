# -*- coding: utf-8 -*-
# @Time    : 2022/3/31 9:33
# @File    : 云上唐河.py
# @Software: PyCharm
""""""
import time
import json
import jsonpath
import requests
import requests_cache as rc

def make_hook(delay=2):
    def hook(response, *args, **kwargs):
        if not getattr(response, 'from_cache', False):
            print('delayTime')
            time.sleep(2)
        else:
            print('Have')
        return response
    return hook
url="https://tanghe.dxhmt.cn/api/sub/article/mergeListNew"
datas={
    'page':	'1',
    'size':'20',
    'pageId':'5cdb3dc65e064d11b8e030ee6e10d07a',
    'appId':'dxrma6af7651c63bbb9efcc15ddf1578',
    'timestamp':'1648695573928',
    'imei':'25c23844-cc1a-4aab-94ba-6a67fe148f76',
    'source':'Android'
}
header={"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.55"}
p = requests.post('https://tanghe.dxhmt.cn/api/sub/article/mergeListNew',data=datas,headers=header)
rc.install_cache()  #初始化爬虫换sun
# rc.clear() #清空爬虫缓存
p.encoding='utf-8'
session = rc.CachedSession()
session.hooks = {'response':make_hook()}
page = session.post(url, data=datas, headers=header)
jsoninfo = json.loads(page.content)
# print(jsoninfo)
with open("D:/DeveloProgram/PythonProgram/第二学期/lian/tanghe.json", "w", encoding="utf-8") as fi:
    fi.write(json.dumps(jsoninfo, indent=2, ensure_ascii=False))


